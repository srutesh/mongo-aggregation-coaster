// Spawn a webpack process
// Start the electron after the webpack-dev-server is started


const { spawn, fork } = require('child_process');
const chokidar = require('chokidar');

let reactProcess, electronProcess, watcher;

function startElectron() {
    electronProcess = spawn('./node_modules/.bin/electron', ['./src/main.js'], {
        stdio: 'inherit'
    });

    electronProcess.on('exit', (code, signal)=>{
        stopProcesses();
    });

    electronProcess.on('error', (error)=>{
        console.log(error);
        stopProcesses();
    });

}

function stopProcesses() {
    if(reactProcess){
        reactProcess.kill('SIGINT');
    }

    if(electronProcess){
        electronProcess.kill('SIGINT');
    }
}

try {
    ['SIGINT', 'SIGTERM'].forEach(function(sig) {
        process.on(sig, function() {
            stopProcesses();
            process.exit();
        });
    });
    
    reactProcess = fork('./scripts/start.js', [], {
        cwd: './src/views/ui',
        stdio: 'inherit',
    })

    reactProcess.on('message', (message)=>{
        switch (message.type) {
            case 'STARTED':
                console.log('React Started');
                startElectron();
                break;
        
            default:
                console.log(`React process says: ${message}`);
                break;
        }
    })

    reactProcess.on('exit', stopProcesses);
    reactProcess.on('error', (error)=>{
        console.log(error);
        stopProcesses();
    });
} catch (error) {
    stopProcesses();
}