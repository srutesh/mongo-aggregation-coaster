import React, { Component } from 'react';
import FormControl from '@material-ui/core/FormControl';
import InputLabel from '@material-ui/core/InputLabel';
import Select from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';
import Button from '@material-ui/core/Button';
import { Redirect } from 'react-router-dom';
import { toast } from 'react-toastify';

class DbList extends Component {
    constructor(props) {
        super(props);

        this.handleDbChange = this.handleDbChange.bind(this);
        this.handleCollectionChange = this.handleCollectionChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);

    }
    
    componentDidMount() {
        this.props.listDbs();
    }

    handleDbChange(event) {
        this.props.onDbChange(event.target.value);
    }

    handleCollectionChange(event) {
        this.props.onCollectionChange(event.target.value)
    }

    handleSubmit(event) {
        event.preventDefault();
        if (this.props.selectedDb.name && this.props.selectedDb.selectedCollection) {
        this.props.onSubmit();
        } else {
            toast("Select a DB and collection to continue.")
        }
    }

    render() {
        if (this.props.loadPlayground) {
            return <Redirect to="/playground"/>
        } else {
            return (
                <React.Fragment>
                    <form onSubmit={this.handleSubmit} noValidate autoComplete="false">
                        <FormControl>
                            <InputLabel htmlFor="database">Database</InputLabel>
                            <Select
                                value={this.props.selectedDb.name}
                                onChange={this.handleDbChange}
                                inputProps={{
                                    id: 'database',
                                }}
                            >
                                <MenuItem value=""><em>None</em></MenuItem>
                                {
                                this.props.dbs.map((db, index) => <MenuItem value={db.name} key={index}><span>{db.name}</span></MenuItem>)
                                }
                            </Select>
                        </FormControl>
                        {
                            this.props.collections && this.props.collections.length ?
                            (
                                <FormControl>
                                    <InputLabel htmlFor="collection">Collection</InputLabel>
                                    <Select
                                        value={this.props.selectedDb.selectedCollection}
                                        onChange={this.handleCollectionChange}
                                        inputProps={{
                                            id: 'collection',
                                        }}
                                    >
                                        <MenuItem value=""><em>None</em></MenuItem>
                                        {
                                            this.props.collections.map((collection, index)=><MenuItem value={collection} key={index}><span>{collection}</span></MenuItem>)
                                        }
                                    </Select>
                                </FormControl>
                            ) : null
                        }
                        <Button type="submit">Load Playground</Button>
                    </form>
                </React.Fragment>
                
            );
        }
    }
}

export default DbList;