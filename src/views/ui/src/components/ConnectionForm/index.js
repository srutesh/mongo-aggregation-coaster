import React, { Component } from 'react';
import Card from '@material-ui/core/Card';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import { Redirect } from "react-router-dom";

class ConnectionForm extends Component {
    constructor(props) {
        super(props);
        this.state = {  };

        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleConnectionUriChange = this.handleConnectionUriChange.bind(this);

    }

    componentDidMount(){
        if (this.props.connection.connected) {
            this.props.history.push('/dbs')
        }
    }

    handleSubmit(event){
        event.preventDefault();
        if(!this.props.connection.connecting){
            this.props.onSubmit(this.props.connection);
        }
    }

    handleConnectionUriChange(event){
        this.props.onConnectionUriChange(event.target.value);
    }
    
    render() {
        if ( this.props.connection.connected) {
            return <Redirect to="/dbs"/>
        } else {
            return (
                <Card>
                    <form onSubmit={this.handleSubmit} noValidate autoComplete="false">
                        <TextField name="uri" label="URI" value={this.props.connection.uri} onChange={this.handleConnectionUriChange}/>
                        <Button type="submit">
                            Connect
                        </Button>
                    </form>
                </Card>
            );
        }
    }
}

export default ConnectionForm;