import React, { Component } from 'react';
import styles from './Main.scss';
import classnames from 'classnames/bind';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import { Route, withRouter } from 'react-router-dom';
import ConnectionForm from '../../containers/ConnectionForm';
import DbList from '../../containers/DbList';
import Playground from '../../containers/Playground';
import { ToastContainer } from 'react-toastify';

const cx = classnames.bind(styles);

class Main extends Component {
    constructor(props) {
        super(props);
    }

    componentDidMount(){
        this.props.history.push("/connect");
    }


    render() {
        return (
            <React.Fragment>
                <ToastContainer/>
                <Route path="/" render={()=>{
                    return (
                        <AppBar position="static" className={cx({ themeRed: true })}>
                            <Toolbar>
                                Mongo Browser
                            </Toolbar>
                        </AppBar>
                    )
                }}/>
                <Route path="/connect" component={ConnectionForm}/>
                <Route path="/playground" component={Playground}/>
                <Route path="/dbs" component={DbList}/>
            </React.Fragment>
        );
    }
}

export default withRouter(Main);
