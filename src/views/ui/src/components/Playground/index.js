import React, { Component } from 'react';
import Button from '@material-ui/core/Button';
import CircularProgress from '@material-ui/core/CircularProgress';
import TextField from '@material-ui/core/TextField';
import FormControl from '@material-ui/core/FormControl';
import FormHelperText from '@material-ui/core/FormHelperText';
import json5 from 'json5';
import Drawer from '@material-ui/core/Drawer';
import classNames from 'classnames/bind';
import styles from './Playground.scss';
import Checkbox from '@material-ui/core/Checkbox';
import Card from '@material-ui/core/Card';
import ReactJson from 'react-json-view';
import { toast } from 'react-toastify';
import CloseIcon from '@material-ui/icons/Close';
import 'react-toastify/dist/ReactToastify.css?raw';

const cx = classNames.bind(styles);

const jsonPreviewProps = {
    iconStyle: "triangle",
    enableClipboard: false,
    displayDataTypes: false,

}


class Playground extends Component {
    constructor(props) {
        super(props);
        this.state = {  };

        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleQueryChange = this.handleQueryChange.bind(this);
        this.setActiveQuery = this.setActiveQuery.bind(this);
        this.addStage = this.addStage.bind(this);
        this.viewResult = this.viewResult.bind(this);
        this.handleActive = this.handleActive.bind(this);
        this.handleInclusion = this.handleInclusion.bind(this);
        this.close = this.close.bind(this)
        this.removeStage = this.removeStage.bind(this)
    }

    // TODO: Add name
    // TODO: Show result screen
    // TODO: Add drag and drop for stages
    // TODO: Implement monaco editor with autocomplete
    // TODO: Add substage support for lookups and facets
    // TODO: Add common date support
    // TODO: Add a button to generate code
    // TODO: Add reselect to gain performance

    handleSubmit(stage, event){
        // TODO: Add a loading indicator
        event.preventDefault();

        let till = this.props.stages.findIndex((stage)=>{
            return stage.active;
        })

        let pipeline;
        if(till + 1 === this.props.stages.length){
            pipeline = this.props.stages;
        } else {
            pipeline = this.props.stages.slice(0, till + 1);
        }

        try {
            let activeStage = json5.parse(stage.query);
            let { db, collection, stages, query} = this.props;
            // this.props.getResult(this.props.query.output, pipeline.map(stage=>json5.parse(stage.query)));
            this.props.getOutput({db, collection, stages, query}, till);
            this.props.setQueryError();
        } catch (error) {
            console.log(error);
            this.props.setQueryError(`Invalid aggregation stage. ${error.message}`);
        }
        
    }

    viewResult(){
        this.props.getResult(this.props.stages, this.props.query.output);
    }
    
    handleQueryChange(event){
        this.props.setQuery(event.target.value);
    }

    setActiveQuery(index){
        // TODO: Show a loading overlay
        // TODO: Clear the inputs and outputs
        // TODO: Validate inputs before switching
        this.props.setActiveQuery(index);
    }

    addStage(){
        for (const [index, stage] of this.props.stages.filter(s=>s.include).entries()) {
            try {
                json5.parse(stage.query)
            } catch (error) {
                toast(`${stage.name || `Stage #${index + 1}`} is invalid. You should exclude it proceed`)
                return;
            }
        }
        this.props.addStage(this.props.stages.length);
        this.props.resetOutput()
    }

    handleActive(index, event){
        if(event.target.checked){
            if(index === this.props.stages.length){
                this.props.setActiveTill()
            } else {
                this.props.setActiveTill(index)
            }
        } else {
            this.props.setInactiveTill(index)
        }
    }

    handleInclusion(index, event){
        event.stopPropagation();

        if(event.target.checked){
            this.props.setInclude(index)
        } else {
            this.props.setExclude(index)
        }

    }

    renderPreview(type){
        return (
            <React.Fragment>
                {
                    this.props.query[type].loading ? <CircularProgress/> : null
                }
                {
                    !this.props.query[type].loading && this.props.query[type].value ?
                    (
                        this.props.query[type].value.map((result, index)=>{
                            return (
                                <div key={index}>   
                                    <ReactJson 
                                        src={result} 
                                        {...jsonPreviewProps}
                                    />
                                </div> 
                            )
                        })
                    )
                    : 
                    <div>
                        Empty
                    </div>
                }
            </React.Fragment>
        )
    }

    close(){
        this.props.close();
        this.props.history.push("/dbs");
    }

    removeStage(event, index){
        event.stopPropagation();
        if(index !== 0){
            let currentActiveIndex = this.props.stages.findIndex(s=>s.active)
            let removedIndex = index;
            
            let setActive;

            if(currentActiveIndex == removedIndex){
                setActive = currentActiveIndex - 1;
            } else {
                setActive = currentActiveIndex
            }

            this.props.removeStage(index, setActive);
        }
    }

    render() {
        // TODO: Add name
        // TODO: Add a blocking loading indicator when performing query
        // TODO: Add page support for input and output
        // TODO: Add react virtualized if needed for input and output
        // TODO: Add ios style diff to input and output
        const stage = this.props.stages.find(s=>s.active);
        let constrainResult = this.props.result.until;
        return (
            <React.Fragment>
                <Drawer variant="permanent" PaperProps={{
                    className: cx('stagePane'),
                    elevation: 8,
                }}>
                    <div>
                        {
                            this.props.stages.map((stage, index)=>{
                                let stageType;

                                try {
                                    let keys = Object.keys(json5.parse(stage.query));
                                    stageType = keys.length ? keys[0].slice(1) : null;
                                } catch (error) {
                                    stageType = null;
                                }

                                return (
                                    <Card 
                                        key={index} 
                                        elevation={stage.active ? 6 : 1}
                                        className={cx('stage', 'd-flex', 'justify-content-between', 'align-items-center', {
                                            'active': stage.active,
                                        })} 
                                        onClick={this.setActiveQuery.bind(null, index)}
                                        >
                                        <Checkbox 
                                            checked={stage.include}
                                            disabled={index === 0}
                                            onChange={this.handleInclusion.bind(null, index)}
                                        />
                                        <div className={cx("flex-grow-1")}>
                                            {stage.name || `Stage #${index + 1}`}
                                            {
                                                stageType ? (
                                                    <small className={cx("mongoStage")}>{stageType}</small>
                                                ) : null
                                            }
                                        </div>
                                        <CloseIcon disabled={index === 0} onClick={(event) => this.removeStage(event, index)}/>
                                    </Card>
                                )
                            })
                        }
                    </div>
                    <Button onClick={this.addStage}>Add stage</Button>
                    <Button onClick={this.viewResult}>View result</Button>
                </Drawer>
                <div className={cx('editorPane')}>
                    {
                        stage ? (
                            <React.Fragment>
                                <button onClick={this.close}>Change DB or collection</button>
                                <div className={cx('editor')}>
                                    <div>Playground</div>
                                    <div>DB: {this.props.db}</div>
                                    <div>Collection: {this.props.collection}</div>
                                    <Button onClick={this.props.getResult}>Evaluate</Button>
                                    <div>
                                    </div>
                                    <form onSubmit={this.handleSubmit.bind(null, stage)}>
                                        <FormControl>
                                            <TextField multiline 
                                                value={stage.query} 
                                                rows="5" cols="10"
                                                onChange={this.handleQueryChange}
                                                />
                                            <FormHelperText>Error</FormHelperText>
                                        </FormControl>
                                        <Button type="submit">Preview</Button>
                                    </form>
                                </div>
                                <div className={cx('preview')}>
                                    <div className={cx('input')}>
                                        <h3>Input</h3>
                                        {this.renderPreview('input')}
                                        
                                    </div>
                                    <div className={cx('output')}>
                                        <h3>Output</h3>
                                        {this.renderPreview('output')}
                                    </div>
                                    
                                </div>
                            </React.Fragment>
                        ) : <div>Loading</div>
                    }                        

                </div>
            </React.Fragment>
        );
    }
}

export default Playground;