import React, { Component } from 'react';
import Main from './components/Main';
import { HashRouter, BrowserRouter } from 'react-router-dom';
import store from './store';
import { Provider } from 'react-redux';
import 'bootstrap/dist/css/bootstrap.min.css?raw'

class App extends Component {
    constructor(props) {
        super(props);
    }



    render() {
        return (
            <BrowserRouter>
                <Provider store={store}>
                    <Main></Main>
                </Provider>
            </BrowserRouter>
        );
    }
}


export default App;
