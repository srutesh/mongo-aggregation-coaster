const {ipcRenderer} = window.require('electron');

const makeIpcCall = (options) => {
    if(beforeCall) {
        beforeCall();
    }

    let defaults = {
        timeout: 60 * 1000,
    }

    let {
        timeout,
        call, data,
        beforeCall,
        onTimeout,
        eventHandlers,
    } = {
        ...defaults, 
        ...options
    };

    // const timeoutDur = timeout * 1000;  // Convert to MS
    // FIXME: Fix duplicate handlers being attached.
    let timeoutId = setTimeout(() => {
        onTimeout()
    }, timeout);

    for (const eventName in eventHandlers) {
        if (eventHandlers.hasOwnProperty(eventName)) {
            const eventHandler = eventHandlers[eventName];
            
            ipcRenderer.once(`${eventName}`, (event, value)=>{
                eventHandler(event, value);
                clearInterval(timeoutId);
            })
        }
    }

    ipcRenderer.send(`${call}`, data);
}

export default makeIpcCall;