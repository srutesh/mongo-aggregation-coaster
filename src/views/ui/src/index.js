import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import store from './store';

const {ipcRenderer} = window.require('electron');

const dispatch = store.dispatch.bind(store);

ipcRenderer.on('CONNECTION_SUCCESS', (event)=>{
    dispatch({
        type: 'CONNECTED',
    })
})


ReactDOM.render(<App />, document.getElementById('root'));
