import { combineReducers } from 'redux';

const initialStates = {
    playground: {
        db: '',
        collection: '',
        aggregation: {
            stages: [
                {
                    name: 'match', 
                    query: "{$match: {}}",
                    active: true,
                    include: true,
                }
            ],
            
            result: {
                value: [],
                until: false,
                loading: false,
            },
        },
        query: {
            input: {
                value: [],
                loading: false,
            },
            output: {
                value: [],
                loading: false,
            },
        }
    }
}

const connection = (state={
    uri: 'mongodb://localhost:27017',
    connected: false,
    connecting: false,
    error: '',
}, action) => {
    switch (action.type) {
        case 'SET_CONNECTION_URI':
            return {
                ...state,
                uri: action.payload.value
            }
        case 'CONNECTING':
            return {
                ...state,
                connecting: false,
                error: '',
            }
        case 'CONNECTED':
            return {
                ...state,
                connected: true,
                connecting: false,
            }
        case 'CONNECTION_ERROR':
            return {
                ...state,
                connected: false,
                connecting: false,
                error: action.payload.value,
            }
        default:
            return state;
    }
}

const server = (state={
    loading: false,
    dbs: [],
    error: '',
    loadPlayground: false,
    selectedDB: {
        name: '',
        collections: [],
        loading: false,
        selectedCollection: '',
    }
}, action) =>{
    switch (action.type) {
        case 'FETCHING_DB_LIST':
            return {
                ...state,
                loading: true
            }
        
        case 'SET_DB_LIST':
            return {
                ...state,
                loading: false,
                dbs: action.payload.value
            }
        
        case 'FETCH_DB_LIST_ERROR':
            return {
                ...state,
                loading: false,
                error: action.payload.value 
            }
        
        case 'FETCHING_COLLECTION_LIST':
            return {
                ...state,
                selectedDB: {
                    ...state.selectedDB,
                    loading: true,
                }
            }
        
        case 'SET_COLLECTION_LIST':
            return {
                ...state,
                selectedDB: {
                    ...state.selectedDB,
                    collections: action.payload.value,
                    loading: false,
                }
            }
        
        case 'SET_DB':
            return {
                ...state,
                selectedDB: {
                    ...state.selectedDB,
                    name: action.payload.value,
                }
            }

        case 'SET_COLLECTION':
            return {
                ...state,
                selectedDB: {
                    ...state.selectedDB,
                    selectedCollection: action.payload.value,
                }
            }
        
        case 'FETCH_COLLECTION_LIST_ERROR':
            return {
                ...state,
                loading: false,
                selectedDB: {
                    ...state.selectedDB,
                    loading: false,
                    error: action.payload.value
                }
            }

        case 'SET_LOAD_PLAYGROUND':
            return {
                ...state,
                loadPlayground: action.payload.value,
            }


        default:
            return state;
    }
}

const resultHandlers = {
    'RESULT': (state, change) => {
        return {
            ...state,
            aggregation: {
                ...state.aggregation,
                result: {
                    ...state.aggregation.result,
                    ...change,
                }
            }
        }
    },
    'INPUT': (state, change) => {
        return {
            ...state,
            query: {
                ...state.query,
                input: {
                    ...state.query.input,
                    ...change,
                }
            },
        }
    },
    'OUTPUT': (state, change) => {
        return {
            ...state,
            query: {
                ...state.query,
                output: {
                    ...state.query.output,
                    ...change,
                }
            },
        }
    }
}

function setActiveStage(state, activeIndex){
    return {
        ...state,
        aggregation: {
            ...state.aggregation,
            stages: state.aggregation.stages.map((stage, index)=>{
                if(activeIndex == index){
                    return {
                        ...stage,
                        active: true,
                    }
                } else if(stage.active){
                    return {
                        ...stage,
                        active: false,
                    }
                }
                else {
                    return stage
                }
            })
        }
        
    }
}

const playground = (state=initialStates.playground, action) => {
    switch (action.type) {
        case 'INIT_PLAYGROUND':
            return {
                ...initialStates.playground,
                db: action.payload.db,
                collection: action.payload.collection,
            }
        
        case 'FETCHING_RESULT': 
            return (()=>{
                let change = {
                    loading: true,
                    value: null,
                }

                return resultHandlers[action.payload.location](state, change);
            })()
        
        case 'FETCH_RESULT_SUCCESS':
            return (()=>{
                let change = {
                    loading: false,
                    value: action.payload.value
                }
                
                return resultHandlers[action.payload.location](state, change);
            })()
            
        case 'FETCH_RESULT_ERROR':
            return (()=>{
                let change = {
                    loading: false,
                    value: null
                }
    
                return resultHandlers[action.payload.location](state, change);
            })()

        case 'SET_QUERY':
            return {
                ...state,
                aggregation: {
                    ...state.aggregation,
                    stages: state.aggregation.stages.map((stage)=>{
                        if(!stage.active) {
                            return stage
                        }

                        return {
                            ...stage,
                            query: action.payload.value,
                        }
                    })
                }
                // query: {
                //     ...state.query,
                //     value: action.payload.value
                // }
            }

        case 'SET_QUERY_ERROR':
            return {
                ...state,
                query: {
                    ...state.query,
                    error: action.payload.value
                }
            }
        
        case 'ADD_STAGE':
            return {
                ...state,
                aggregation: {
                    ...state.aggregation,
                    stages: state.aggregation.stages.map((stage)=>{
                        if(stage.active){
                            return {
                                ...stage,
                                active: false,
                            }
                        }

                        return stage
                    }).concat([
                        {
                            name: '', 
                            query: '',
                            active: true,
                            include: true,
                        }
                    ])
                },
            }
        
        case 'SET_ACTIVE_QUERY':
            return setActiveStage(state, action.payload.value)
            

        case 'SET_ACTIVE_TILL':
            return {
                ...state,
                aggregation: {
                    ...state.aggregation,
                    result: {
                        ...state.result,
                        until: action.payload.value,
                    }
                }
            }

        case 'SET_INCLUDE':
            return {
                ...state,
                aggregation: {
                    ...state.aggregation,
                    stages: state.aggregation.stages.map((stage, index) => {
                        if(index === action.payload.value){
                            stage.include = true
                        }
                        return stage
                    })
                }
            }
        
        case 'SET_EXCLUDE':
            return {
                ...state,
                aggregation: {
                    ...state.aggregation,
                    stages: state.aggregation.stages.map((stage, index) => {
                        if(index === action.payload.value){
                            stage.include = false
                        }
                        return stage
                    })
                }
            }

        case 'REMOVE_STAGE':
            let intermediateState = action.payload.setActive === false ? state : setActiveStage(state, action.payload.setActive);
            
            return {
                ...intermediateState,
                aggregation: {
                    ...intermediateState.aggregation,
                    stages: intermediateState.aggregation.stages.filter((stage, index) => {
                        return index !== action.payload.index
                    })
                }
            }

        default:
            return state
    }
}

const rootReducer = combineReducers({
    connection,
    server,
    playground,
})

export default rootReducer;
