import { connect } from "react-redux";
import { withRouter } from "react-router-dom";
import ConnectionForm from '../components/ConnectionForm';

const {ipcRenderer} = window.require('electron');

export default withRouter(connect((state)=>({
    connection: state.connection
}), (dispatch)=>({
    onConnectionUriChange(value){
        dispatch({
            type: 'SET_CONNECTION_URI',
            payload: { value }
        })
    },
    onSubmit(data){
        if(data.connecting){
            return;
        }
        return dispatch((dispatch) => {
            dispatch({
                type: 'CONNECTING',
            });

            const timeout = 60 * 1000;

            let timeoutId = setTimeout(() => {
                dispatch({
                    type: 'CONNECTION_ERROR',
                    payload: {
                        value: 'No response'
                    }
                });
            }, timeout);

            ipcRenderer.on('CONNECTION_SUCCESS', (event)=>{
                dispatch({
                    type: 'CONNECTED',
                });

                clearInterval(timeoutId);
            })

            ipcRenderer.on('CONNECTION_ERROR', (event, error)=>{
                dispatch({
                    type: 'CONNECTION_ERROR',
                    payload: {
                        value: error.message,
                    }
                });

                clearInterval(timeoutId);
            })

            ipcRenderer.send('CONNECT', data);

        })
    },
}))(ConnectionForm))