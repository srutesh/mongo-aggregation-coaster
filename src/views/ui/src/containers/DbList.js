import { connect } from "react-redux";
import { withRouter } from "react-router-dom";
import DbList from '../components/DbList';
import makeIpcCall from "../utils/makeIpcCall";

const {ipcRenderer} = window.require('electron');

export default withRouter(connect((state)=>({
    dbs: state.server.dbs,
    selectedDb: state.server.selectedDB,
    collections: state.server.selectedDB.collections,
    loadPlayground: state.server.loadPlayground,
}), (dispatch)=>({

    setLoadPlayground(value){
        dispatch({
            type: 'SET_LOAD_PLAYGROUND',
            payload: {
                value,
            }
        })
    },

    onSubmit({db, collection}){
        dispatch({
            type: 'SET_LOAD_PLAYGROUND',
            payload: {
                value: true,
            }
        })

        dispatch({
            type: 'INIT_PLAYGROUND',
            payload: {
                db, collection,
            }
        })
    },

    onCollectionChange(value){
        dispatch({
            type: 'SET_COLLECTION',
            payload: {
                value,
            }
        })
    },
    
    onDbChange(selectedDb, name){
        if(selectedDb.loading){
            return;
        }
        return dispatch((dispatch)=>{
            
            dispatch({
                type: 'SET_DB',
                payload: {
                    value: name,
                }
            })

            let eventHandlers = {
                FETCH_COLLECTION_LIST_SUCCESS: (event, value)=>{
                    dispatch({
                        type: 'SET_COLLECTION_LIST',
                        payload: {
                            value,
                        }
                    });
    
                },
                FETCH_COLLECTION_LIST_ERROR: (event, value)=>{
                    dispatch({
                        type: 'SET_COLLECTION_LIST',
                        payload: {
                            value,
                        }
                    });
                }
            }

            makeIpcCall({
                call: 'LIST_COLLECTIONS',
                data: {name},
                beforeCall: ()=>{
                    dispatch({
                        type: 'FETCHING_COLLECTION_LIST',
                    });
                },
                onTimeout: () => {
                    dispatch({
                        type: 'FETCH_COLLECTION_LIST_ERROR',
                        payload: {
                            value: 'No response'
                        }
                    });
                },
                eventHandlers,
            })

            // dispatch({
            //     type: 'FETCHING_COLLECTION_LIST',
            // });

            

            // const timeout =  60 * 1000;

            // let timeoutId = setTimeout(() => {
            //     dispatch({
            //         type: 'FETCH_COLLECTION_LIST_ERROR',
            //         payload: {
            //             value: 'No response'
            //         }
            //     });
            // }, timeout);

            // ipcRenderer.on('FETCH_COLLECTION_LIST_SUCCESS', (event, value)=>{
            //     dispatch({
            //         type: 'SET_COLLECTION_LIST',
            //         payload: {
            //             value,
            //         }
            //     });

            //     clearInterval(timeoutId);
            // })

            // ipcRenderer.on('FETCH_COLLECTION_LIST_ERROR', (event, error)=>{
            //     dispatch({
            //         type: 'FETCH_COLLECTION_LIST_ERROR',
            //         payload: {
            //             value: error.message,
            //         }
            //     });

            //     clearInterval(timeoutId);
            // })

            // ipcRenderer.send('LIST_COLLECTIONS', {name});
        })
    },
    listDbs(data){
        if(data.loading){
            return;
        }

        return dispatch((dispatch)=>{
            dispatch({
                type: 'FETCHING_DB_LIST',
            });

            const timeout =  60 * 1000;

            let timeoutId = setTimeout(() => {
                dispatch({
                    type: 'FETCH_DB_LIST_ERROR',
                    payload: {
                        value: 'No response'
                    }
                });
            }, timeout);

            ipcRenderer.on('FETCH_DB_LIST_SUCCESS', (event, value)=>{
                dispatch({
                    type: 'SET_DB_LIST',
                    payload: {
                        value,
                    }
                });

                clearInterval(timeoutId);
            })

            ipcRenderer.on('FETCH_DB_LIST_ERROR', (event, error)=>{
                dispatch({
                    type: 'FETCH_DB_LIST_ERROR',
                    payload: {
                        value: error.message,
                    }
                });

                clearInterval(timeoutId);
            })

            ipcRenderer.send('LIST_DBS', data);
        })
    },
}), (stateProps, dispatchProps, ownProps)=>{
    dispatchProps.listDbs = dispatchProps.listDbs.bind(null, {
        loading: stateProps.loading,
    })

    // const _onDbChange = dispatchProps.onDbChange.bind(null);

    // dispatchProps.onDbChange = (value) => _onDbChange(stateProps.selectedDb, value)

    return {
        ...ownProps,
        ...stateProps,
        ...dispatchProps,
        onDbChange: dispatchProps.onDbChange.bind(null, stateProps.selectedDb),
        onSubmit: dispatchProps.onSubmit.bind(null, {
            db: stateProps.selectedDb.name, 
            collection: stateProps.selectedDb.selectedCollection
        })
    }
})(DbList))