import { connect } from "react-redux";
import { withRouter } from "react-router-dom";
import Playground from '../components/Playground';
import makeIpcCall from '../utils/makeIpcCall';
import json5 from 'json5';
import { toast } from 'react-toastify';

const {ipcRenderer} = window.require('electron');

const provideContext = (stateProps, target) => {
    const context = {
        db: stateProps.db, 
        collection: stateProps.collection,
        stages: stateProps.stages, 
        query: stateProps.query,
    }
    return (...args) => target(context, ...args)
}

export default withRouter(connect((state)=>({
    db: state.playground.db,
    collection: state.playground.collection,
    stages: state.playground.aggregation.stages,
    result: state.playground.aggregation.result,
    query: state.playground.query,
}), (dispatch)=>({
    getResult({db, collection}, result, stages, options={
        location: 'RESULT',
    }){
        if(result.loading){
            return;
        }
        
        return dispatch((dispatch)=>{

            let payload = {
                location: options.location,
            }

            makeIpcCall({
                call: 'FETCH_RESULT',
                // Tell the main process the location of output
                // so that we can conditionally fire dispatches.
                data: {db, collection, stages, location: options.location},
                beforeCall: ()=>{
                    dispatch({
                        type: 'FETCHING_RESULT',
                        payload: {
                            ...payload,
                        }
                    });
                },
                onTimeout: () => {
                    dispatch({
                        type: 'FETCH_RESULT_ERROR',
                        payload: {
                            ...payload,
                            value: 'No response',
                        }
                    });
                },
                eventHandlers: {
                    [`FETCH_RESULT_SUCCESS_${options.location}`]: (event, value)=>{
                        dispatch({
                            type: 'FETCH_RESULT_SUCCESS',
                            payload: {
                                ...payload,
                                value,
                            }
                        });
                    },
                    [`FETCH_RESULT_ERROR_${options.location}`]: (event, error)=>{
                        dispatch({
                            type: 'FETCH_RESULT_ERROR',
                            payload: {
                                ...payload,
                                value: error.message,
                            }
                        });
                        toast(error.message)
                    },
                }
            })

            
        })
    },

    setQuery(value){
        dispatch({
            type: 'SET_QUERY',
            payload: {
                value,
            }
        })
    },

    get getInput(){
        return ({db, collection, stages, query}, index, stagesOverride) => {
            if(stagesOverride){
                stages = stagesOverride;
            } else {
                stages = stages.slice(0, index)
            }
            this.getResult({db, collection}, query.input, stages.filter((s) => {
                return s.include
            }).map((s)=>{
                return json5.parse(s.query)
            }), {
                location: 'INPUT',
            })
        }
    },

    get addStage() {
        return (context, index) =>{
            dispatch({
                type: 'ADD_STAGE',
            });
            
            this.getInput(context, index);
        }
    },
    
    get getOutput(){
        return ({db, collection, stages, query}, index, stagesOverride) => {
            if(stagesOverride){
                stages = stagesOverride;
            } else {
                stages = stages.slice(0, index + 1)
            }
            this.getResult({db, collection}, query.output, stages.filter((s) => {
                return s.include
            }).map((s)=>{
                return json5.parse(s.query)
            }), {
                location: 'OUTPUT',
            })
        }
    },

    setQueryError(value){
        dispatch({
            type: 'SET_QUERY_ERROR',
            payload: {
                value,
            }
        })
        if(value){
            toast(value)
        }
    },

    resetOutput(){
        dispatch({
            type: 'FETCH_RESULT_SUCCESS',
            payload: {
                location: 'OUTPUT',
                value: [],
            }
        });
    },

    resetInput(){
        dispatch({
            type: 'FETCH_RESULT_SUCCESS',
            payload: {
                location: 'INPUT',
                value: [],
            }
        });
    },

    get setActiveQuery(){
        return (context, value)=>{
            dispatch({
                type: 'SET_ACTIVE_QUERY',
                payload: {
                    value,
                }
            })


            this.getInput(context, value)

            try {
                json5.parse(context.stages[value].query);
                this.resetOutput();
                this.getOutput(context, value)
            } catch (error) {
                // TODO: Set error
            }
        }

        
    },

    setActiveTill(value=false){
        dispatch({
            type: 'SET_ACTIVE_TILL',
            payload: {
                value,
            }
        })
    },

    setInactiveTill(value){
        dispatch({
            type: 'SET_ACTIVE_TILL',
            payload: {
                value: value - 1,
            }
        })
    },

    setInclude(value){
        dispatch({
            type: 'SET_INCLUDE',
            payload: {
                value,
            }
        })
    },

    setExclude(value){
        dispatch({
            type: 'SET_EXCLUDE',
            payload: {
                value,
            }
        })
    },
    
    close() {
        dispatch({
            type: 'SET_LOAD_PLAYGROUND',
            payload: {
                value: false,
            }
        })
    },

    get removeStage() {
        return (context, index, setActive) => {
            dispatch({
                type: 'REMOVE_STAGE',
                payload: {
                    index,
                    setActive,
                }
            })

            const inputStages = context.stages.slice(0, setActive).filter((s, i) => {
                return i !== index
            })
            const outputStages = context.stages.slice(0, setActive + 1).filter((s, i) => {
                return i !== index
            })

            this.getInput(context, null, inputStages)
            this.getOutput(context, null, outputStages)
            
        }
    },

}), (stateProps, dispatchProps, ownProps)=>{
    
    return {
        ...stateProps,
        ...dispatchProps,
        ...ownProps,
        getResult: provideContext(stateProps, dispatchProps.getResult),
        addStage: provideContext(stateProps, dispatchProps.addStage),
        removeStage: provideContext(stateProps, dispatchProps.removeStage),
        setActiveQuery: provideContext(stateProps, dispatchProps.setActiveQuery),
    }
})(Playground))