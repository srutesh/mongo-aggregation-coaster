const electron = require('electron');
const mongodb = require('mongodb');
const MongoClient = mongodb.MongoClient;
const ipcMain = electron.ipcMain;
const bson = require('bson');
const _ = require('lodash');

let client, db;

// TODO: Configure Logging

function transformResult(obj) {
    return _.transform(obj, function(result, value, key) { // transform to a new object

        // Show ObjectID's as strings
        if (value instanceof mongodb.ObjectID) {
            result[key] = `ObjectID(${value.toHexString()})`
        } else {
            result[key] = _.isObject(value) ? transformResult(value) : value; // if the key is an object run it through the inner function
        }

    });
}

ipcMain.on('CONNECT', async (event, data)=>{
    let publisher = event.sender;
    if(client){
        // TODO: Handle previously open connection properly
        client.close();
        db = null;

    }

    try {
        client = await MongoClient.connect(data.uri);    
        publisher.send('CONNECTION_SUCCESS')
    } catch (error) {
        publisher.send('CONNECTION_ERROR', {
            message: error.message, stack: error.stack,
        })
    }
})

ipcMain.on('LIST_DBS', async (event)=>{
    let publisher = event.sender;

    if(!client){
        publisher.send('FETCH_DB_LIST_ERROR', new Error("No connection"))
    } else {
        try {
            let adminDb = client.db('test').admin();
            let dbs = await adminDb.listDatabases();
    
            publisher.send('FETCH_DB_LIST_SUCCESS', dbs.databases);
            
        } catch (error) {
            publisher.send('FETCH_DB_LIST_ERROR', error)
        }
    }
})

ipcMain.on('LIST_COLLECTIONS', async (event, {name})=>{
    let publisher = event.sender;

    if(!client){
        publisher.send('FETCH_COLLECTION_LIST_ERROR', new Error("No connection"))
    } else {
        try {
            let db = client.db(name);
            let collections = await db.collections();
    
            publisher.send('FETCH_COLLECTION_LIST_SUCCESS', collections.map(c=>c.collectionName));
            
        } catch (error) {
            publisher.send('FETCH_COLLECTION_LIST_ERROR', error)
        }
    }
})

const locationTagFactory = (location) => (strs, ...substs) => {
    let result = strs.raw[0];
    for (const [i,subst] of substs.entries()) {
        result += subst;
        result += strs.raw[i+1];
    }
    return `${result}_${location}`;
}

ipcMain.on('FETCH_RESULT', async (event, {db, collection, stages, location})=>{
    let publisher = event.sender;
    let tagger = locationTagFactory(location)
    if(!client){
        publisher.send(tagger`FETCH_RESULT_ERROR`, new Error("No connection"))
    } else {
        try {
            let col = client.db(db).collection(collection);
            let agCursor = await col.aggregate(stages);

            
            let results = [];
            let i = 5;

            while(i--){
                results.push(agCursor.next());
            }

            results = await Promise.all(results)
            
            bson;
            mongodb;
            _;

            results = results.map(r => transformResult(r));
            
            publisher.send(tagger`FETCH_RESULT_SUCCESS`, results);

            
        } catch (error) {
            publisher.send(tagger`FETCH_RESULT_ERROR`, {
                message: error.message, stack: error.stack,
            })
        }
    }
})